<h1>Docker Summary 

definitions:  
**Docker** is a software Developement platform that allows us to develop and deploy apps inside container environments.

**Docker Image** is a package of all the tools one would need to run applications.

![Architecture of Container](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png?itok=vle7kjDj)
_________________________________________________________________________________

<h2>Docker Image

image contains requirements to run application and is deployed to Docker as container  
Requirements:  
-code  
-runtime  
-libraries  
-env variables  
-config files  
_________________________________________________________________________________

<h2>Containers

Each have a specific job, their own operating system, their own isolated cpu 
processes, memory and cpu resources. 
Running docker image  
Can create multiple containers  

**Docker Hub**  
is an online repository of the available cloud containers which is 
preconfigured for their preferred programming language such as ruby or nodejs
_________________________________________________________________________________

<h2>How it works?

![How it works](https://geekflare.com/wp-content/uploads/2019/09/docker-architecture.png)


where Docker Daemon is the server  
and the Rest API is used to instruct the docker  
and the Command Line Interface is used to enter the commands into the docker  
_________________________________________________________________________________

<h2>Basic Commands

includes:  
1. view all the containers running in Docker Host  
  '_$ docker ps_'  
2. starts the container specified  
  '_$ docker start 30986_' or '_docker start sneha_'  
3. stops the container specified  
  '_$ docker stop 30986_'  
4. creates the container from images  
  '_$ docker run sneha_'  
5. deletes the container  
  '_$ docker rm sneha_'  
6. **downloading** the docker  
  '_docker pull snehabhapkar/trydock_'  
7. Run docker image  
  '_docker run -ti snehabhapkar/trydock /bin/bash_'  
8. to **copy file** inside a container  
  '_docker cp hello.py e0b72ff850f8:/_'  
9. **gives permission**  and **installs dependencies**  
  '_docker exec -it e0b72ff850f8 chmod +x requirements.sh_'  
  or  
  '_docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh_'  
10. **Run** program inside container  
  '_docker start e0b72ff850f8_'  
  or  
  '_docker exec e0b72ff850f8 python3 hello.py_'  
11. **Save** program in docker image  
  '_docker commit e0b72ff850f8 snehabhapkar/trydock_'  
12. **Change image name**  
  '_docker tag snehabhapkar/trydock username/repo_'  
13. **Push** on docker image  
  '_docker push username/repo_'  
