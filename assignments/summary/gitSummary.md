<h1>Git Summary

_______________________________________________________________________________  

<h2>Git

**Git** is version-control software that records all the changes made to a file by one or more than one person.
![Difference between Git and Github](https://blog.devmountain.com/wp-content/uploads/2019/07/Gitvs.Github-1a.jpg)
_________________________________________________________________________________

<h3>Gitlab

open source code repository  
**Terminology:**    
1. Merge Request: request to merge a feature branch into official master   
2. Snippet: Can be shared public, internal or private  
3. Project: is a container including Git repo, discussions, attachments, project-specific settings  
4. Groups: projects can be added here for managemenet by a group of people with feature of notifications  

**Git Terminology**  


1. Repository/Repo:  
collection of files/folders 
2. Commit:  
Saving the work on your remote repository  
3. Push:  
Updating commits onto Gitlab  
4. Gitlab:  
remote storage solution  
5. Pull:  
Updating local version of repo to current version  
6. Branch:  
separate intstances different from main code  
7. Merge:  
integrating branches/put into primary codebase  
8. Clone:  
copies repo onto local machine 
9. Fork:  
duplicate exisitng repo as an entirely new repo under your name  
_________________________________________________________________________________

<h3>Installation of Git:

'sudo apt-get install git'  
_________________________________________________________________________________

<h3>Git Internals

Stages:  
1. **Modified**: changed file, but not commited  
2. **Staged**: file ready to commit  
3. **Comitted**: Data stored in local repo

![Flow of software code](https://querix.com/go/beginner/Content/Resources/Images/05_workbench/01_ls/04_how_to/10_repos/git_files/git_00_intro_01_sections.png)
_________________________________________________________________________________

<h3>Git workflow  

Some of the commands in Git workflow include:  
1. Clone repo:  
  '$ git clone <link-to-repository>'  
2. New branch:  
  '$ git checkout master'  
  '$ git checkout -b <your-branch-name>'  
3. Add untracked files:  
  '$ git add'  
4. Commit  
  '$ git commit -sv'  
5. Push:  
  '$ git push origin <branch-name>'  

![Comprehensive flowchart](https://miro.medium.com/max/875/1*a9cDsKIG0JFXkN0yVfG82Q.png)

_________________________________________________________________________________

<h3>Markdown Language Summary

**Syntax cheat sheet:**
1. _italics_:underscore to change words to italics('_'words'_')
2. **bold**: double asterisk to make words bold('**'words'**')
3. #header: Number of hash corresponds to header(cannot make bold)
   if heading 1 then '#'
   if heading 2 then '##' and so forth
4. To quote a block: '>'
5. Unordered list: '-'
6. Ordered list: '1.' and so on  
7. To hyperlink: Put words in square brackets followed by link in curved brackets  
8. To insert image: type '!' followed by alt text in square brackets and link in curved brackets  
